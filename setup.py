import setuptools 

with open("README.md", "r") as fh:     
    long_description = fh.read() 

setuptools.setup(     
    name = "bld_tbt",     
    version = "1.0.0",     
    author = "Laura Torino",     
    author_email = "laura.torino@esrf.fr",     
    description = "Set and show the SR BLDs in turn by turn mode",     
    long_description = long_description,     
    long_description_content_type = "text/markdown",     
    url = "http://packages.python.org/an_example_pypi_project",     
    packages=['bld_tbt'],     
    include_package_data = True,     
    classifiers=[         
        "Programming Language :: Python :: 3",         
        "License :: GPL",         
        "Operating Systems :: OS Independant",     
    ], 
)
